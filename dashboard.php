<?php
ini_set('session.cookie_httponly', 1);
session_start();

if ( isset($_GET['logout']) || !(isset($_SESSION['ip_address']) || isset($_SESSION['port']) || isset($_SESSION['password'])) ) {
	session_destroy();
	header("Location: index.php");
	die;
}
include 'includes/query.php';
if ( !isset($_SESSION['refreshTime']) || time() - $_SESSION['refreshTime'] >= 300 || isset($_POST['refresh']) && time() - $_SESSION['refreshTime'] >= 90 ) {
	$_SESSION['onlinePlayers'] = getServerPlayers();
	getInfoServer($_SESSION['ip_address'], $_SESSION['port']); // refresh topbar infos
	if ( $_SESSION['onlinePlayers'] !== array() ) // remove cooldown when there's an error
		$_SESSION['refreshTime'] = time();
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Serveur Manager</title>
	<link rel="stylesheet" href="css/dashboard.min.css">
	<script src="js/application.min.js" type="text/javascript" charset="utf-8" async defer></script>
	<meta charset="utf-8">
</head>
<body>
	<center>
	<ul class="ul-topbar">
		<li><span><?php echo htmlspecialchars($_COOKIE['sName']); ?></span></li>
		<li><span><?php echo htmlspecialchars($_COOKIE['sPlayer'].' / '.$_COOKIE['sPlayers'].' Joueurs'); ?></span></li>
		<li><span><?php echo htmlspecialchars($_SESSION['ip_address'].':'.$_SESSION['port']); ?></span></li>
		<li><span><?php echo htmlspecialchars($_COOKIE['sMap']); ?></span></li>
	</ul>
	</center>
	<ul class="ul-sidebar">
		<li><a class="a-sidebar" href="#">Console</a></li>
		<li><a class="a-sidebar" href="#plymanage">Manage Player</a></li>
		<li><a class="a-sidebar logout" href="?logout">Disconnect</a></li>
	</ul>
	<div id="content">
		<div id="terminal" class="current-category">
			<span>Console</span>
			<div></div>
			<input type="text" id="terminal-input" placeholder="Command...">
			<button id="terminal-submit">Execute</button>
		</div>
		<div id="plymanage">
			<table>
				<tr>
					<th>Nick</th>
					<th>Score</th>
					<th>Time connected</th>
					<th>Actions</th>
				</tr>
			<?php 
				foreach ( $_SESSION['onlinePlayers'] as $player ) {
					$player['name'] = htmlspecialchars($player['name'], ENT_QUOTES);
					echo '<tr><td>'.$player['name'].'</td><td>'.$player['score'].'</td><td>'.$player['time'].'</td>'.'<td><button onclick="MANAGER_GLOBAL.showBankickWindow(true,\''.$player['name'].'\')">Kick</button><button onclick="MANAGER_GLOBAL.showBankickWindow(false,\''.$player['name'].'\')">Ban</button>';
				}
			?>
			</table>
			<form method="post">
				<button name="refresh">Refresh</button>
			</form>
			<div>
				<div>
					<h1>Kick options<span onclick="animModal(false)">&times;</span></h1>
					<input type="text" placeholder="Reason">
					<input type="number" min="0" placeholder="Time">
					<input type="button" value="Validate">
				</div>
			</div>
		</div>
	</div>
</body>
</html>