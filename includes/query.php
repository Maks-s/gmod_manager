<?php

//-----------------------------------------------------
// Get basic infos about server, list players
//-----------------------------------------------------

function getInfoServer($IP, $Port) {
	$blacklistPort = array(
		80,
		443,
		5353,
		631,
		5900
	);
	if ( isset($blacklistPort[$Port]) )
		return -2;
	$Socket = @fsockopen($IP, $Port, $errno, $errstr, 10);
	if ( $Socket === false ) // check if host is up
		return $errno;
	fclose($Socket);
	$Socket = @fsockopen("udp://".$IP, $Port, $errno, $errstr);
	stream_set_timeout($Socket, 2, 1000);
	stream_set_blocking($Socket, true);
	fwrite($Socket, "\xFF\xFF\xFF\xFF\x54Source Engine Query\x00");
	$packet = @explode("\x00", substr(fread($Socket, 4096), 6), 5);
	if ( count($packet) !== 5 )
		return -2;
	setcookie("sName", $packet[0]);
	setcookie("sMap", $packet[1]);
	setcookie("sPlayer", ord(substr($packet[4], 2, 1)));
	setcookie("sPlayers", ord(substr($packet[4], 3, 1)));
	fclose($Socket);
	return 0;
}

function getServerPlayers() {
	if ( !isset($_SESSION['password']) || !isset($_SESSION['ip_address']) || !isset($_SESSION['port']) ) {
		die;
	}
	$Socket = @fsockopen("udp://".$_SESSION['ip_address'], $_SESSION['port'], $errno, $errstr);
	if ( $Socket === false ) {
		return array();
	}
	stream_set_timeout($Socket, 2, 1000);
	stream_set_blocking($Socket, true);
	fwrite($Socket, "\xFF\xFF\xFF\xFF\x55\xFF\xFF\xFF\xFF"); // get challenge
	$packet = @substr(fread($Socket, 9), 5);
	if ( $packet === false ) {
		return array();
	}
	fwrite($Socket, "\xFF\xFF\xFF\xFF\x55".$packet); // return challenge
	$packet = @substr(fread($Socket, 4096), 5);
	if ( $packet === false ) {
		return array();
	}
	fclose($Socket);
	$playerCount = ord($packet[0]);
	$packet = substr($packet, 1);
	$output = array();
	for ($i=0; $i < $playerCount; $i++) {
		$packet = substr($packet, 1); // remove player id
		$output[$i] = array();
		$output[$i]['name'] = @iconv( 'UTF-8', 'UTF-8//IGNORE', substr($packet, 0, strpos($packet,"\x00")) ); // filter invalid caracters
		if ( $output[$i]['name'] === '' ) { // doesn't count connecting players
			unset($output[$i]);
			$packet = substr($packet, strpos($packet,"\x00")+9);
			continue;
		}
		$packet = substr($packet, strpos($packet,"\x00")+1);
		$scoreTime = unpack('l1score/f1time', $packet);
		$packet = substr($packet, 8);
		$output[$i]['score'] = $scoreTime['score'];
		$output[$i]['time'] = date("H:i:s",$scoreTime['time']);
	}
	return $output;
}