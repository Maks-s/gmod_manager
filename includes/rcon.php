<?php

//-----------------------------------------------------
// Check RCON password, send commands
//-----------------------------------------------------

// Protect against CSRF attacks
if ( get_included_files()[0] === __FILE__ && (empty($_SERVER['HTTP_X_REQUESTED_WITH']) || $_SERVER['HTTP_X_REQUESTED_WITH'] !== "YourOwnWebsite") ) {
    die;
}

function checkRconPassword($IP, $Port, $Password) {
	$Socket = @fsockopen($IP, $Port, $errno, $errstr, 10);
	if ( $Socket === false ) {
		return $errno;
	}
	stream_set_timeout($Socket,2,500);
	$data = pack("VV",1,3).$Password."\x00\x00";
	$data = pack("V",strlen($data)).$data;
	fwrite($Socket,$data,strlen($data));
	$retarray = array();
	while ($size = fread($Socket,4)) {
		$size = unpack('V1Size',$size);
		if ($size["Size"] > 4096) {
			$packet = "\x00\x00\x00\x00\x00\x00\x00\x00".fread($Socket,4096);
		} else {
			$packet = fread($Socket,$size["Size"]);
		}
		array_push($retarray,unpack("V1ID/V1Response/a*S1/a*S2",$packet));
	}
	if ( !isset($retarray[1]['ID']) ) {
		return -2;
	}
	if ( $retarray[1]['ID'] !== 1) {
		return -3;
	}
	fclose($Socket);
	$_SESSION['password'] = $Password;
	$_SESSION['ip_address'] = $IP;
	$_SESSION['port'] = $Port;
	return 0;
}

function sendRconCommand($Command) {
	if ( !isset($_SESSION['password']) || !isset($_SESSION['ip_address']) || !isset($_SESSION['port']) ) {
		die;
	}
	if ( strlen($Command) > 20000 ) {
		die('Command too long<br>');
	}
	$Socket = @fsockopen($_SESSION['ip_address'], $_SESSION['port'], $errno, $errstr, 30);
	if ( $Socket === false ) {
		echo  "Error #$errno<br>";
		return;
	}
	stream_set_timeout($Socket,2,500);
	$data = "\x01\x00\x00\x00\x03\x00\x00\x00".$_SESSION['password']."\x00\x00";
	$data = pack("V",strlen($data)).$data;
	fwrite($Socket,$data,strlen($data)); // login
	$data = "\x02\x00\x00\x00\x02\x00\x00\x00".$Command."\x00\x00";
	$data = pack("V",strlen($data)).$data;
	fwrite($Socket,$data,strlen($data)); // send command
	$Packets = array();
	while ($size = @fread($Socket,4)) {
		$size = unpack('V1',$size);
		if ($size[1] > 4096) {
			$packet = @fread($Socket,4096)."\x00\x00\x00\x00\x00\x00\x00\x00";
		} else {
			$packet = @fread($Socket,$size[1]);
		}
		array_push($Packets,unpack("V1ID/V1Response/a*S1/a*S2",$packet));
	}
	fclose($Socket);
	foreach($Packets as $pack) {
		if (isset($ret[$pack['ID']])) {
			$ret[$pack['ID']]['S1'] .= $pack['S1'];
			$ret[$pack['ID']]['S2'] .= $pack['S1'];
		} else {
			$ret[$pack['ID']] = array(
				'Response' => $pack['Response'],
				'S1' => $pack['S1'],
				'S2' =>	$pack['S2']
			);
		}
	}
	echo @nl2br(htmlspecialchars($ret[2]["S1"]));
}

if ( !empty($_POST['command']) && get_included_files()[0] === __FILE__ ) {
	@session_start();
	sendRconCommand($_POST['command']);
}