<?php
ini_set('session.cookie_httponly', 1);
session_start();

if ( !empty($_POST['serverip']) && !empty($_POST['serverpassword']) ) {
	$octet = explode(".", stripslashes($_POST['serverip']));
	if ( !(isset($octet[0]) && isset($octet[1]) && isset($octet[2]) && isset($octet[3]) && count($octet) === 4) )
		$error = -1;
	else {
		$port = explode(":", $octet[3]);
		if ( !(isset($port[0]) && isset($port[1]) && count($port) === 2 && $port[1] > 1 && $port[1] < 65535) ) // 65535 is max port
			$error = -2;
		elseif ( !( is_numeric($octet[0]) && is_numeric($octet[1]) && is_numeric($octet[2]) && is_numeric($port[0]) ) ||
				!($octet[0] < 255 && $octet[0] > 0 && $octet[1] < 255 && $octet[1] > 0 && $octet[2] < 255 && $octet[2] > 0 && $port[0] < 255 && $port[0] > 0) ) {
			$error = -1;
		}
	}
	if ( !isset($error) ) {
		include 'includes/query.php';
		$finalIP = $octet[0].'.'.$octet[1].'.'.$octet[2].'.'.$port[0];
		$error = getInfoServer($finalIP, $port[1]);
		if ( $error === 0 ) {
			include 'includes/rcon.php';
			$error = checkRconPassword($finalIP, $port[1], $_POST['serverpassword']);
			if ( $error === 0 ) {
				header('Location: dashboard.php');
				exit();
			}
		}
	}
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>Serveur Manager</title>
	<link rel="stylesheet" type="text/css" href="css/index.min.css">
	<meta charset="UTF-8">
</head>
<body>
	<center>
	<div class="loginform">
		<h1 class="title">Server Manager</h1>
		<?php if ( isset($error) ) { ?>
			<div class="error"><span><?php
			if ( $error === -1 ) echo "Invalid IP";
			elseif ( $error === -2 ) echo "Invalid Port";
			elseif ( $error === -3 ) echo "Wrong password";
			elseif ( $error === 10060 || $error === 10061 ) echo "Connection error";
			else echo "Erreur";
			?></span></div>
		<?php }
			if ( isset($_SESSION['ip_address']) && isset($_SESSION['port']) && isset($_SESSION['password']) ) { ?>
				<center><a href="dashboard.php" class="comeback">Recover current session</a></center>
			<?php } ?>
		<span class="undertext">Enter the server's IP:Port then its RCON Password to take remote control</span>
		<form method="post" accept-charset="utf-8">
			<input class="inputtext" type="text" name="serverip" placeholder="IP:Port"><br />
			<input class="inputtext" type="password" name="serverpassword" placeholder="Rcon"><br/>
			<input class="submit" type="submit" value="Log in">	
		</form>
	</div>
	</center>
</body>
</html>