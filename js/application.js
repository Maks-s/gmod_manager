var MANAGER_GLOBAL = {
    httpRequest: new XMLHttpRequest(),

    doTheFade: function(element) {
        var categoryCurrent = document.getElementsByClassName("current-category")[0];
        categoryCurrent.classList.remove('current-category');
        categoryCurrent.classList.add('fading-out');
        setTimeout(function() {
            categoryCurrent.style.display = "none";
            element.style.display = "block";
            categoryCurrent.classList.remove('fading-out');
            element.classList.add("fading-in");
            setTimeout(function() {
                element.classList.remove("fading-in");
                element.classList.add('current-category');
                element.style.display = "block";
            }, 200);
        }, 200);
    },

    hashChanged: function(hash, lastHash) {
        if ( document.getElementsByClassName("current-category").length !== 1) { // protect against 2fast clickers
            window.location.hash = lastHash;
            return;
        }
        for ( var i=0; i<MANAGER_GLOBAL.a_sidebar.length; i++ ) {
            MANAGER_GLOBAL.a_sidebar[i].classList.remove('current-sidebar');
            if ( MANAGER_GLOBAL.a_sidebar[i].href.split('#')[1] == hash.substr(1) ) {
                MANAGER_GLOBAL.a_sidebar[i].classList.add('current-sidebar');
            }
        }
        if ( hash === "#plymanage" ) {
            MANAGER_GLOBAL.doTheFade(MANAGER_GLOBAL.plymanage);
        } else {
            MANAGER_GLOBAL.doTheFade(MANAGER_GLOBAL.terminal);
        }
    },

    escapeHTML: function(text, inversecap = false) {
        if ( inversecap ) {
            var unescape = {
                "&amp;": '&',
                "&lt;": '<',
                "&gt;": '>',
                '&quot;': '"',
                "&#039;": "'"
            };
            return text.replace(/&quot;|&#039|&amp;|&lt;|&gt;/g, function(m) { return unescape[m]; });
        }
        var escape = {
            '"': '&quot;',
            "'": "&#039;",
            "&": "&amp;"
        };
        return text.replace(/["'&]/g, function(m){return escape[m];});
    },

    banKickPlymanage: function(doKick, name, time = 0) {
        MANAGER_GLOBAL.httpRequest.onreadystatechange = function() {
            if ( this.readyState === 4 && (this.status === 200 || this.status === 0) ) {
                MANAGER_GLOBAL.terminal_output.innerHTML = MANAGER_GLOBAL.terminal_output.innerHTML+this.responseText;
                MANAGER_GLOBAL.terminal_output.scrollTop = MANAGER_GLOBAL.terminal_output.scrollHeight;
            }
        };
        MANAGER_GLOBAL.httpRequest.open("POST", "includes/rcon.php");
        MANAGER_GLOBAL.httpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
        MANAGER_GLOBAL.httpRequest.setRequestHeader("X-Requested-With", "YourOwnWebsite");
        if ( doKick ) {
            MANAGER_GLOBAL.terminal_output.innerHTML = MANAGER_GLOBAL.terminal_output.innerHTML+'<span class="terminal-command">ulx kick "'+name+'" "'+MANAGER_GLOBAL.escapeHTML(MANAGER_GLOBAL.banKickReason.value)+'"</span><br />';
            MANAGER_GLOBAL.httpRequest.send("command="+encodeURIComponent('ulx kick "'+MANAGER_GLOBAL.escapeHTML(name, true)+'" "'+MANAGER_GLOBAL.banKickReason.value+'"'));
        } else {
            MANAGER_GLOBAL.terminal_output.innerHTML = MANAGER_GLOBAL.terminal_output.innerHTML+'<span class="terminal-command">ulx ban "'+name+' '+time+' "'+MANAGER_GLOBAL.escapeHTML(MANAGER_GLOBAL.banKickReason.value)+'"</span><br />';
            MANAGER_GLOBAL.httpRequest.send("command="+encodeURIComponent('ulx ban "'+MANAGER_GLOBAL.escapeHTML(name, true)+'" '+time+' "'+MANAGER_GLOBAL.banKickReason.value+'"'));
        }
    },

    animModal: function(appear) {
        if ( appear ) {
            MANAGER_GLOBAL.modalBankick.classList.add("fading-in");
            MANAGER_GLOBAL.modalBankick.style.display = "block";
            setTimeout(function() {
                MANAGER_GLOBAL.modalBankick.classList.remove("fading-in");
            }, 200);
        } else {
            MANAGER_GLOBAL.modalBankick.classList.add("fading-out");
            setTimeout(function() {
                MANAGER_GLOBAL.modalBankick.style.display = "none";
                MANAGER_GLOBAL.modalBankick.classList.remove("fading-out");
            }, 200);
        }
    },

    showBankickWindow: function(doKick, name) {
        MANAGER_GLOBAL.banKickReason.value = "";
        MANAGER_GLOBAL.banKickTime.value = "";
        if ( doKick ) {
            MANAGER_GLOBAL.banKickValidate.onclick = function() {
                MANAGER_GLOBAL.banKickPlymanage(true, name);
                MANAGER_GLOBAL.animModal(false);
            };
            MANAGER_GLOBAL.banKickTime.style.display = "none";
            MANAGER_GLOBAL.banKickTitle.innerHTML = 'Kick options<span onclick="MANAGER_GLOBAL.animModal(false)">&times;</span>';
        } else {
            MANAGER_GLOBAL.banKickValidate.onclick = function() {
                MANAGER_GLOBAL.banKickPlymanage(false, name, MANAGER_GLOBAL.banKickTime.value);
                MANAGER_GLOBAL.animModal(false);
            };
            MANAGER_GLOBAL.banKickTime.style.display = "inline";
            MANAGER_GLOBAL.banKickTitle.innerHTML = 'Ban options<span onclick="MANAGER_GLOBAL.animModal(false)">&times;</span>';
        }
        MANAGER_GLOBAL.animModal(true);
    },

    init: function() {
        // -----------[[ SETUP VAR ]]----------- //
        MANAGER_GLOBAL.terminal = document.getElementById("terminal");
        MANAGER_GLOBAL.terminal_output = MANAGER_GLOBAL.terminal.getElementsByTagName("div")[0];
        MANAGER_GLOBAL.a_sidebar = document.getElementsByClassName("a-sidebar");
        MANAGER_GLOBAL.plymanage = document.getElementById("plymanage");
        MANAGER_GLOBAL.modalBankick = MANAGER_GLOBAL.plymanage.getElementsByTagName("div")[0];
        MANAGER_GLOBAL.banKickReason = MANAGER_GLOBAL.modalBankick.getElementsByTagName("input")[0]; // make them global so we don't run getElement each time
        MANAGER_GLOBAL.banKickTime = MANAGER_GLOBAL.modalBankick.getElementsByTagName("input")[1];
        MANAGER_GLOBAL.banKickValidate = MANAGER_GLOBAL.modalBankick.getElementsByTagName("input")[2];
        MANAGER_GLOBAL.banKickTitle = MANAGER_GLOBAL.modalBankick.getElementsByTagName("h1")[0];

        // -----------[[ SETUP SIDEBAR ]]----------- //
        var target = "";
        if ( window.location.hash === "#plymanage" ) {
            target = "plymanage";
        }
        if ( target !== "" ) {
            MANAGER_GLOBAL.terminal.style.display = "none";
            MANAGER_GLOBAL.terminal.classList.remove('current-category');
            var theCategory = document.getElementById(target);
            theCategory.style.display = "block";
            theCategory.classList.add("current-category");
        }
        for ( var i=0; i<MANAGER_GLOBAL.a_sidebar.length; i++ ) {
            if ( MANAGER_GLOBAL.a_sidebar[i].href.split('#')[1] === target ) {
                MANAGER_GLOBAL.a_sidebar[i].classList.add("current-sidebar");
            }
        }

        // -----------[[ SETUP CONSOLE ]]----------- //
        var consoleInput = document.getElementById("terminal-input");
        var consoleButton = document.getElementById("terminal-submit");
        consoleInput.addEventListener("keyup", function(event) {
            if ( event.keyCode == 13 ) {
                consoleButton.click();
            }
        });
        consoleButton.onclick = function() {
            if ( consoleInput.value === "" ) {
                return;
            }
            consoleInput.disabled = true;
            consoleButton.disabled = true;
            MANAGER_GLOBAL.terminal_output.innerHTML = MANAGER_GLOBAL.terminal_output.innerHTML+"<span class=\"terminal-command\">"+consoleInput.value+"</span><br />";
            MANAGER_GLOBAL.terminal_output.scrollTop = MANAGER_GLOBAL.terminal_output.scrollHeight; // auto scroll
            MANAGER_GLOBAL.httpRequest.onreadystatechange = function() {
                if ( this.readyState === 4 && (this.status === 200 || this.status === 0) ) {
                    consoleInput.disabled = false;
                    consoleButton.disabled = false;
                    MANAGER_GLOBAL.terminal_output.innerHTML = MANAGER_GLOBAL.terminal_output.innerHTML+this.responseText;
                    MANAGER_GLOBAL.terminal_output.scrollTop = MANAGER_GLOBAL.terminal_output.scrollHeight;
                }
            };
            MANAGER_GLOBAL.httpRequest.open("POST", "includes/rcon.php");
            MANAGER_GLOBAL.httpRequest.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
            MANAGER_GLOBAL.httpRequest.setRequestHeader("X-Requested-With", "YourOwnWebsite");
            MANAGER_GLOBAL.httpRequest.send("command="+encodeURIComponent(consoleInput.value));
            consoleInput.value = "";
        };

        // -----------[[ SETUP PLYMANAGE ]]----------- //
        MANAGER_GLOBAL.modalBankick.onclick = function(event) {
            if (event.target == this) {
                MANAGER_GLOBAL.animModal(false);
            }
        };
    }

};

MANAGER_GLOBAL.init();

if ("onhashchange" in window) {
    var lastHash = window.location.hash;
    window.onhashchange = function() {
        MANAGER_GLOBAL.hashChanged(window.location.hash, lastHash);
        lastHash = window.location.hash;
    };
} else {
    var hashStored = window.location.hash;
    window.setInterval( function() {
        if (window.location.hash !== hashStored) {
            MANAGER_GLOBAL.hashChanged(window.location.hash, hashStored);
            hashStored = window.location.hash;
        }
    }, 100);
}